\section*{Particle Swarm Optimization}

\noindent
La heurística que lleva por nombre Particle Swarm Optimization (o PSO) fue originada a partir de una serie de deducciones que sostenían, entre otras cosas, que los miembros de un grupo pueden beneficiarse del conocimiento adquirido por sus compañeros, y así obtener una ventaja evolutiva. Considerando variables como el ambiente en el que el grupo está, la velocidad a la que se desplazan, y la búsqueda multi-dimensional -ya sea que hablemos de espacios tridimensionales, como el espacio, o de espacios más abstractos-, se creó la versión inicial de PSO en 1995\cite{PSO}. Posteriormente, en 1998, los mismos autores publicaron una modificación de PSO, que incluía nuevas características para el mejoramiento de las soluciones obtenidas.\cite{PSOv2}\\

Derivado de lo anterior, hablamos de PSO como un procedimiento para encontrar la mejor solución a un problema de optimización, donde se poseé un conjunto de partículas que se mueven por el espacio de soluciones (visto como un espacio n-dimensional) a cierta velocidad; y además, cada partícula toma en consideración las mejores soluciones encontradas por él mismo, y las encontradas por los demás miembros del conjunto.\\

Antes de describir el procedimiento general de PSO, veremos qué es una particula.

\subsection*{Particula}

\noindent 
Una particula es un elemento del grupo que participará en la búsqueda del objetivo. En este caso específico, es una posible solución al problema. Está compuesto por cinco elementos:

\begin{itemize}
    \item \textbf{Su posición}: Es la potencial solución óptima al problema, y es representada como un vector de \textit{n} dimensiones, que es un lugar en el espacio de soluciones (n-dimensional).
    
    \item \textbf{Su valor}: El valor de su posición, de acuerdo a la función de optmización diseñada para el problema que se desea resolver.
    
    \item \textbf{Su velocidad}: La dirección e intensidad del movimiento.
    
    \item \textbf{La mejor posición}: La mejor posición encontrada por esa partícula.
    
    \item \textbf{El mejor valor}: El valor de la mejor posición encontrada por esa partícula.
\end{itemize}

\subsection*{Actualización de partículas}

\noindent
En cada paso o iteración de la ejecución de la heurística, la posición y velocidad de cada partícula debe ser actualizada. Sea \textit{t} la iteración que acaba de terminar, podemos determinar la nueva posición y velocidad de cada partícula en la iteración \textit{t+1} acuerdo a las siguientes fórmulas:

\[
v_i(t+1) = w*v_i(t) + c_1*r_1*(p_i - x_i(t)) + c_2*r_2*(p_g - x_i(t)) \label{eq:velocidad} \tag{1}
\]

\[
x_i(t+1) = x_i(t) + v_i(t+1) \label{eq:posicion} \tag{2}
\]

Donde:
\begin{itemize}
    \item $v_i(t)$ representa a la velocidad de la partícula $i$ en la iteración $t$
    \item $x_i(t)$ representa a la posición de la partícula $i$ en la iteración $t$
    \item $w$ es el peso de la inercia
    \item $c_1$ y $c_2$ son constantes de aceleración
    \item $r_1$ y $r_2$ son números aleatorios entre 0 y 1
    \item $p_i$ es la mejor solución encontrada por la partícula $i$
    \item $p_g$ es la mejor solución encontrada por todas las partículas hasta la iteración actual\\
\end{itemize}

\noindent
Respecto a la ecuación que determina la velocidad de la siguiente iteración, cada uno de los tres sumandos posee un significado en el planteamiento de PSO.

\begin{enumerate}
    \item Regula la velocidad obtenida en la iteración anterior. Decimos que esto es la \textit{inercia} del movimiento de las partículas. Sin este sumando, las partículas tenderían a moverse a posiciones muy cercanas a su posición inicial, y parecería un algoritmo de búsqueda local. Al adicionar la velocidad, contemplamos la posibilidad de que las partículas extiendan su búsqueda a otras áreas. 
    \item Distancia entre la mejor posición obtenida por la misma partícula y su posición actual. Este es el \textit{componente cognitivo}, o la consideración de su mejor solución para decidir su siguiente posición.
    \item Distancia entre la mejor posición obtenida por todas las partículas (la mejor solución global hasta la iteración anterior) y su posición actual. Este es el \textit{componente social}, que es la consideración de los resultados obtenidos por el resto del grupo para decidir su siguiente posición. 
\end{enumerate}

\noindent
El significado e importancia de algunas de estas variables se explicarán en la siguiente sección.


\subsection*{Parámetros de la heurística}

\begin{itemize}
    \item \textbf{Mejor solución global}: La mejor solución encontrada por todas las partículas a lo largo de las iteraciones realizadas hasta el momento. Se utiliza en la actualización de las velocidades de las partículas, tan como nuestra la ecuación \eqref{eq:velocidad}.

    \item \textbf{Peso (\textit{w})}: Valor que afecta al primer sumando de la ecuación \eqref{eq:velocidad}. Proporciona un balance (no total) entre la búsqueda local y global. Por lo explicado en el funcionamiento de la ecuación, la exploración es proporcional al valor de \textit{w}. Puede permanecer estática a lo largo de la ejecución de la heurística, o variar a través de ella.
    
    \item \textbf{Constantes de aceleración $\pmb{c_1}$ y $\pmb{c_2}$}: Cada constante pertenece a su respectivo sumando en la ecuación \eqref{eq:velocidad}. Otorga importancia a la distancia actual de una partícula respecto a su mejor posición encontrada, o a la mejor encontrada globalmente. 
    
    \begin{itemize}
        \item[$\longrightarrow$] Si $c_1 = 0$ y $c_2 = 2$, se elimina la consideración de la partícula por su propia mejor solución encontrada, y sólo se considera la mejor conseguida por todos. En este caso, las partículas convergen rápidamente hacia un óptimo, que puede ser local o global. Casi no hay exploración del espacio de soluciones, pero sí explotación del área cercana al óptimo focalizado.
        
        \item[$\longrightarrow$] Si $c_1 = 2$ y $c_2 = 0$, las partículas sólo tomarán en cuenta a sus mejores soluciones, y no convergerán a un punto. Decimos que la exploración es amplia (por la repartición esparcida de las partículas), pero la explotación es baja.
    \end{itemize}
    
    \noindent
    Es recomendado establecer ambas constantes con un valor de 2.
    
    \item \textbf{Velocidades mínima y máxima}: Utilizadas para controlar la velocidad con la que se mueven las partículas. A pesar de que se recomienda usarlas en la implementación, no se encontró en la literatura alguna aplicación o fórmula modificada que las contemple (\cite{PSO} y \cite{PSOv2}).
    
    \item \textbf{Número de partículas}.
    
    \item \textbf{Cantidad de iteraciones}: Iteraciones para la ejecución de la heurística.
\end{itemize}

\subsection*{Diagrama de flujo de la heurística}

\noindent
A continuación se presenta el diagrama de flujo de la heurística PSO. Los detalles de la implementación se establecerán en otra sección.

\begin{figure}
    \centering
    \includegraphics[scale=0.45]{imagenes/pso.png}
    \caption{Diagrama de flujo de PSO}
    \label{fig:my_label}
\end{figure}