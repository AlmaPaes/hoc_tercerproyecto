PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE knapsack (
	id	INTEGER PRIMARY KEY,
	capacidad DOUBLE
);

CREATE TABLE item (
    id INTEGER PRIMARY KEY,
    beneficio DOUBLE,
    peso DOUBLE
);

INSERT INTO knapsack VALUES(1, 1008);

INSERT INTO item VALUES(1,416,248);
INSERT INTO item VALUES(2,992,421);
INSERT INTO item VALUES(3,649,322);
INSERT INTO item VALUES(4,237,795);
INSERT INTO item VALUES(5,457,43);
INSERT INTO item VALUES(6,815,845);
INSERT INTO item VALUES(7,446,955);
INSERT INTO item VALUES(8,422,252);
INSERT INTO item VALUES(9,791,9);
INSERT INTO item VALUES(10,359,901);
INSERT INTO item VALUES(11,667,122);
INSERT INTO item VALUES(12,598,94);
INSERT INTO item VALUES(13,506,326);
INSERT INTO item VALUES(14,94,485);
INSERT INTO item VALUES(15,7,738);
INSERT INTO item VALUES(16,544,574);
INSERT INTO item VALUES(17,334,715);
INSERT INTO item VALUES(18,766,882);
INSERT INTO item VALUES(19,994,367);
INSERT INTO item VALUES(20,893,984);
INSERT INTO item VALUES(21,633,299);
INSERT INTO item VALUES(22,131,433);
INSERT INTO item VALUES(23,428,682);
INSERT INTO item VALUES(24,700,72);
INSERT INTO item VALUES(25,617,874);
INSERT INTO item VALUES(26,874,138);
INSERT INTO item VALUES(27,720,856);
INSERT INTO item VALUES(28,419,145);
INSERT INTO item VALUES(29,794,995);
INSERT INTO item VALUES(30,196,529);
INSERT INTO item VALUES(31,997,199);
INSERT INTO item VALUES(32,116,277);
INSERT INTO item VALUES(33,908,97);
INSERT INTO item VALUES(34,539,719);
INSERT INTO item VALUES(35,707,242);
INSERT INTO item VALUES(36,569,107);
INSERT INTO item VALUES(37,537,122);
INSERT INTO item VALUES(38,931,70);
INSERT INTO item VALUES(39,726,98);
INSERT INTO item VALUES(40,487,600);
INSERT INTO item VALUES(41,772,645);
INSERT INTO item VALUES(42,513,267);
INSERT INTO item VALUES(43,81,972);
INSERT INTO item VALUES(44,943,895);
INSERT INTO item VALUES(45,58,213);
INSERT INTO item VALUES(46,303,748);
INSERT INTO item VALUES(47,764,487);
INSERT INTO item VALUES(48,536,923);
INSERT INTO item VALUES(49,724,29);
INSERT INTO item VALUES(50,789,674);
INSERT INTO item VALUES(51,479,540);
INSERT INTO item VALUES(52,142,554);
INSERT INTO item VALUES(53,339,467);
INSERT INTO item VALUES(54,641,46);
INSERT INTO item VALUES(55,196,710);
INSERT INTO item VALUES(56,494,553);
INSERT INTO item VALUES(57,66,191);
INSERT INTO item VALUES(58,824,724);
INSERT INTO item VALUES(59,208,730);
INSERT INTO item VALUES(60,711,988);
INSERT INTO item VALUES(61,800,90);
INSERT INTO item VALUES(62,314,340);
INSERT INTO item VALUES(63,289,549);
INSERT INTO item VALUES(64,401,196);
INSERT INTO item VALUES(65,466,865);
INSERT INTO item VALUES(66,689,678);
INSERT INTO item VALUES(67,833,570);
INSERT INTO item VALUES(68,225,936);
INSERT INTO item VALUES(69,244,722);
INSERT INTO item VALUES(70,849,651);
INSERT INTO item VALUES(71,113,123);
INSERT INTO item VALUES(72,379,431);
INSERT INTO item VALUES(73,361,508);
INSERT INTO item VALUES(74,65,585);
INSERT INTO item VALUES(75,486,853);
INSERT INTO item VALUES(76,686,642);
INSERT INTO item VALUES(77,286,992);
INSERT INTO item VALUES(78,889,725);
INSERT INTO item VALUES(79,24,286);
INSERT INTO item VALUES(80,491,812);
INSERT INTO item VALUES(81,891,859);
INSERT INTO item VALUES(82,90,663);
INSERT INTO item VALUES(83,181, 88);
INSERT INTO item VALUES(84,214,179);
INSERT INTO item VALUES(85,17,187);
INSERT INTO item VALUES(86,472,619);
INSERT INTO item VALUES(87,418,261);
INSERT INTO item VALUES(88,419,846);
INSERT INTO item VALUES(89,356,192);
INSERT INTO item VALUES(90,682,261);
INSERT INTO item VALUES(91,306,514);
INSERT INTO item VALUES(92,201,886);
INSERT INTO item VALUES(93,385,530);
INSERT INTO item VALUES(94,952,849);
INSERT INTO item VALUES(95,500,294);
INSERT INTO item VALUES(96,194,799);
INSERT INTO item VALUES(97,737,391);
INSERT INTO item VALUES(98,324,330);
INSERT INTO item VALUES(99,992,298);
INSERT INTO item VALUES(100,224,790);
INSERT INTO item VALUES(101,260,275);
INSERT INTO item VALUES(102,97,826);
INSERT INTO item VALUES(103,210,72);
INSERT INTO item VALUES(104,649,866);
INSERT INTO item VALUES(105,919,951);
INSERT INTO item VALUES(106,63,748);
INSERT INTO item VALUES(107,958,685);
INSERT INTO item VALUES(108,804,956);
INSERT INTO item VALUES(109,518,564);
INSERT INTO item VALUES(110,428,183);
INSERT INTO item VALUES(111,537,400);
INSERT INTO item VALUES(112,346,721);
INSERT INTO item VALUES(113,153,207);
INSERT INTO item VALUES(114,971,323);
INSERT INTO item VALUES(115,662,611);
INSERT INTO item VALUES(116,197,116);
INSERT INTO item VALUES(117,91,109);
INSERT INTO item VALUES(118,529,795);
INSERT INTO item VALUES(119,126,343);
INSERT INTO item VALUES(120,747,862);
INSERT INTO item VALUES(121,469,685);
INSERT INTO item VALUES(122,770,10);
INSERT INTO item VALUES(123,934,881);
INSERT INTO item VALUES(124,723,984);
INSERT INTO item VALUES(125,895,403);
INSERT INTO item VALUES(126,568,360);
INSERT INTO item VALUES(127,172,449);
INSERT INTO item VALUES(128,958,541);
INSERT INTO item VALUES(129,383,272);
INSERT INTO item VALUES(130,308,877);
INSERT INTO item VALUES(131,970,359);
INSERT INTO item VALUES(132,583,707);
INSERT INTO item VALUES(133,48,308);
INSERT INTO item VALUES(134,930,770);
INSERT INTO item VALUES(135,569,30);
INSERT INTO item VALUES(136,3,208);
INSERT INTO item VALUES(137,20,311);
INSERT INTO item VALUES(138,609,100);
INSERT INTO item VALUES(139,887,939);
INSERT INTO item VALUES(140,825,422);
INSERT INTO item VALUES(141,930,785);
INSERT INTO item VALUES(142,904,370);
INSERT INTO item VALUES(143,241,989);
INSERT INTO item VALUES(144,379,969);
INSERT INTO item VALUES(145,376,143);
INSERT INTO item VALUES(146,962,972);
INSERT INTO item VALUES(147,889,28);
INSERT INTO item VALUES(148,443,61);
INSERT INTO item VALUES(149,216,638);
INSERT INTO item VALUES(150,338,348);
INSERT INTO item VALUES(151,160,347);
INSERT INTO item VALUES(152,406,66);
INSERT INTO item VALUES(153,159,391);
INSERT INTO item VALUES(154,31,638);
INSERT INTO item VALUES(155,204,295);
INSERT INTO item VALUES(156,420,826);
INSERT INTO item VALUES(157,153,196);
INSERT INTO item VALUES(158,425,449);
INSERT INTO item VALUES(159,331,855);
INSERT INTO item VALUES(160,565,143);
INSERT INTO item VALUES(161,838,487);
INSERT INTO item VALUES(162,9,140);
INSERT INTO item VALUES(163,918,564);
INSERT INTO item VALUES(164,533,615);
INSERT INTO item VALUES(165,232,135);
INSERT INTO item VALUES(166,957,564);
INSERT INTO item VALUES(167,591,360);
INSERT INTO item VALUES(168,576,793);
INSERT INTO item VALUES(169,746,163);
INSERT INTO item VALUES(170,377,859);
INSERT INTO item VALUES(171,858,760);
INSERT INTO item VALUES(172,86,711);
INSERT INTO item VALUES(173,434,662);
INSERT INTO item VALUES(174,558,159);
INSERT INTO item VALUES(175,279,660);
INSERT INTO item VALUES(176,840,268);
INSERT INTO item VALUES(177,735,948);
INSERT INTO item VALUES(178,574,315);
INSERT INTO item VALUES(179,126,676);
INSERT INTO item VALUES(180,912,341);
INSERT INTO item VALUES(181,739,689);
INSERT INTO item VALUES(182,821,894);
INSERT INTO item VALUES(183,625,706);
INSERT INTO item VALUES(184,917,490);
INSERT INTO item VALUES(185,201,478);
INSERT INTO item VALUES(186,993,671);
INSERT INTO item VALUES(187,149,932);
INSERT INTO item VALUES(188,52,899);
INSERT INTO item VALUES(189,759,237);
INSERT INTO item VALUES(190,267,187);
INSERT INTO item VALUES(191,256,472);
INSERT INTO item VALUES(192,783,772);
INSERT INTO item VALUES(193,117,98);
INSERT INTO item VALUES(194,516,906);
INSERT INTO item VALUES(195,180,911);
INSERT INTO item VALUES(196,25,635);
INSERT INTO item VALUES(197,380,225);
INSERT INTO item VALUES(198,712,823);
INSERT INTO item VALUES(199,266,164);
INSERT INTO item VALUES(200,216,343);
COMMIT;