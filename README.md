# HOC_TercerProyecto

Seminario de Heurísticas y Optimización Combinatoria 2021-1\
Proyecto 3\
 :school_satchel: 0-1 Knapsack con Particle Swarm Optimization  :penguin:

## Autora
Alma Rosa Páes Alcalá :beetle:

---

## Versiones de software
* **rustc** `1.47.0`
* **cargo** `1.47.0`
* Versiones de dependencias utilizadas se encuentran en el archivo [**Cargo.toml**](Cargo.toml)

# Requerimientos

La instancia debe estar almacenada en una base de datos, la cual debió ser creada con SQLite. A su vez, la base de datos debe ser almacenada en la carpeta [instancias](instancias).


## Modo de uso
**Para compilar:** `cargo build`\
**Para correr las pruebas:** `cargo test` \
**Para correr el programa:** 
`cargo run --release {semilla} -i {numero de la instancia}`
