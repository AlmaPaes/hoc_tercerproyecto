use rand::prelude::*;
use crate::instancia::*;
use std::sync::Arc;
use std::fmt;

#[derive(Clone)]
pub struct Particula {
    pub instancia: Arc<Instancia>,
    pub posicion: Vec<i32>,
    pub velocidad: Vec<f64>,
    pub valor: f64,
    pub mejor_posicion: Vec<i32>,
    pub mejor_valor: f64
}

pub fn valor_inicial(pos_inicial: &Vec<i32>, instancia: Arc<Instancia>) -> (f64,f64) {
    let mut valor = 0.0;
    let mut costo = 0.0;
    for t in pos_inicial.iter().zip(instancia.items.iter()) {
        if *t.0 == 1 {
            valor += t.1.beneficio;
            costo += t.1.peso;
        }
    }
    (valor,costo)
}

fn get_modulo(a: i32, b: i32) -> i32 {
    ((a % b) + b) % b
}

impl fmt::Debug for Particula {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Particula")
        .field("mejor posicion", &self.mejor_posicion)
        .field("mejor valor", &self.mejor_valor)
        .finish()
    }
}

impl Particula {
    pub fn new(instancia: Arc<Instancia>, minima_velocidad: f64, maxima_velocidad: f64, rng: &mut rand_chacha::ChaCha8Rng) -> Particula {
        let mut pos: Vec<i32> = Vec::new();
        let mut random; 
        for _i in 0..instancia.items.len() {
            random = rng.gen_range(0.0,1.0);
            if random < 0.5 {
                pos.push(0);
            } else {
                pos.push(1);
            }
        }

        let pos2 = pos.clone();

        let mut vel: Vec<f64> = Vec::new();
        for _j in 0..instancia.items.len() {
            random = rng.gen_range(0.0,1.0);
            vel.push(minima_velocidad + (random * (maxima_velocidad - minima_velocidad)));
        }

        let (v,c) = valor_inicial(&pos, Arc::clone(&instancia));
        let espacio = instancia.mochila - c;
        let valor_init = 
            if espacio >= 0.0 {
                v
            } else {
                v*espacio
            };

        Particula {
            instancia,
            posicion: pos,
            velocidad: vel,
            valor: valor_init,
            mejor_posicion: pos2,
            mejor_valor: valor_init
        }

    }

    

    pub fn actualizar_posicion(&mut self, inercia: f64, constante1: f64, constante2: f64, 
        mejor_grupal: &Vec<i32>, rng: &mut rand_chacha::ChaCha8Rng) {
        let mut v;
        let mut x;
        let mut m;
        for i in 0..self.velocidad.len() {
            v = self.velocidad[i];
            x = self.posicion[i];
            m = mejor_grupal[i];
            self.velocidad[i] = inercia * v + 
                                (constante1 * rng.gen_range(0.0,1.0) * (self.mejor_posicion[i] as f64 - x as f64)) +
                                (constante2 * rng.gen_range(0.0,1.0) * (m as f64 - x as f64));
            self.posicion[i] = get_modulo(x + self.velocidad[i] as i32, 2);
        }

        self.recalcular_valor();
        if self.valor > self.mejor_valor {
            self.mejor_posicion = self.posicion.clone();
            self.mejor_valor = self.valor;
        }
    }

    pub fn recalcular_valor(&mut self) {
        let (v,c) = valor_inicial(&self.posicion, Arc::clone(&self.instancia));

        let espacio = self.instancia.mochila - c;
        if espacio < 0.0{
            self.valor = v * espacio;
        } else {
            self.valor = v;
        }
    }
}

#[cfg(test)]
mod tests{
    use crate::instancia::*;
    use crate::particula::*;
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;
    use std::rc::Rc;
    use std::sync::{Arc, Mutex};

    #[test]
    fn test_create_particula() {
        let semilla: u64 = 2;
        let mut rng : rand_chacha::ChaCha8Rng = <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);

        let instancia2 = Arc::new(Instancia::new(2));
        let particula2 = Particula::new(Arc::clone(&instancia2), 0.1, 3.0, &mut rng);
        particula2.posicion.iter().for_each(|x| assert!(*x == 0 || *x == 1) );
        assert_eq!(particula2.valor, -40667128.0);
        assert_eq!(particula2.valor, particula2.mejor_valor);

        let instancia3 = Arc::new(Instancia::new(3));
        let particula3 = Particula::new(Arc::clone(&instancia3), 0.1, 3.0, &mut rng);
        particula3.posicion.iter().for_each(|x| assert!(*x == 0 || *x == 1) );
        assert_eq!(particula3.valor,-2258047764.0);
        assert_eq!(particula3.valor, particula3.mejor_valor);
    }

    #[test]
    fn test_actualiza_posicion() {
        let instancia = Arc::new(Instancia::new(2));
        let semilla: u64 = 2;
        let mut rng : rand_chacha::ChaCha8Rng = <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);
        let mut particula = Particula::new(Arc::clone(&instancia), 0.1, 3.0, &mut rng);
        let particula2 = Particula::new(Arc::clone(&instancia), 0.1, 3.0, &mut rng);
        let valor_anterior = particula.valor;
        particula.actualizar_posicion(0.8, 2.0, 2.0, &particula2.posicion, &mut rng);
        particula.posicion.iter().for_each(|x| assert!(*x == 0 || *x == 1) );
        let valor_siguiente = particula.valor;
        if valor_anterior >= valor_siguiente {
            assert_eq!(valor_anterior,particula.mejor_valor);
        } else {
            assert_eq!(valor_siguiente, particula.mejor_valor);
        }
    }
}