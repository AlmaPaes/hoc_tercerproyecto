use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use std::sync::Arc;

use crate::instancia::*;
use crate::particula::*;

pub struct Optimizacion {
    instancia: Arc<Instancia>,
    num_particulas: i32,
    constante1: f64,
    constante2: f64,
    velocidad_minima: f64,
    velocidad_maxima: f64,
    num_iteraciones: i32,
    inercia_inicial: f64,
    inercia_final: f64,
    rnd_generator: rand_chacha::ChaCha8Rng,
}

impl Optimizacion {
    pub fn new(instancia: Arc<Instancia>, num_particulas: i32,
                constante1: f64, constante2: f64,
                velocidad_minima: f64, velocidad_maxima: f64,
                inercia_inicial: f64, inercia_final: f64,
                num_iteraciones: i32, semilla: u64) -> Optimizacion {
        let rng = <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);
        Optimizacion {
            instancia,
            num_particulas,
            constante1,
            constante2,
            velocidad_minima,
            velocidad_maxima,
            inercia_inicial,
            inercia_final,
            num_iteraciones,
            rnd_generator: rng
        }
    }

    fn get_mejor_particula(&self, particulas: &Vec<Particula>) -> Particula {
        let mut mejor = 0;
        let mut valor = particulas[0].mejor_valor;
        for i in 0..particulas.len() {
            if particulas[i].mejor_valor > valor {
                mejor = i;
                valor = particulas[i].mejor_valor;
            }
        }
        particulas[mejor].clone()
    }

    pub fn pso_heuristic(&mut self) -> Particula {
        let mut particulas: Vec<Particula> = Vec::new();
        for _i in 0..self.num_particulas {
            particulas.push(Particula::new(Arc::clone(&self.instancia), self.velocidad_minima,self.velocidad_maxima, &mut self.rnd_generator));
        }

        let mut mejor_solucion = self.get_mejor_particula(&particulas);
        let mut inercia = self.inercia_inicial;
        let avance_inercia = (self.inercia_inicial - self.inercia_final) / self.num_iteraciones as f64;
        let mut iteracion = 1;
        while iteracion < self.num_iteraciones {
            //println!("inercia: {:?}",inercia);
            for p in particulas.iter_mut() {
                p.actualizar_posicion(inercia, self.constante1, self.constante2, &mejor_solucion.mejor_posicion, &mut self.rnd_generator);
            }
            mejor_solucion = self.get_mejor_particula(&particulas);
            inercia -= avance_inercia;
            iteracion += 1;
        }

        mejor_solucion
    }
} 