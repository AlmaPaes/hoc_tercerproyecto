use crate::database::*;

#[derive(Clone, Debug)]
pub struct Item {
    //pub id: i32,
    pub beneficio: f64,
    pub peso: f64
}

#[derive(Clone,Debug)]
pub struct Instancia {
    pub mochila: f64,
    pub items: Vec<Item>
}

impl Instancia {
    pub fn new(num_instancia: i32) -> Instancia {
        let conexion = db_connection(num_instancia);
        Instancia {
            mochila: get_capacity(&conexion),
            items: get_items(&conexion)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::instancia::*;

    #[test]
    fn test_get_instancias() {
        let instancia1: Instancia = Instancia::new(1);
        let instancia2: Instancia = Instancia::new(2);
        let instancia3: Instancia = Instancia::new(3);
        let instancia4: Instancia = Instancia::new(4);    


        assert_eq!(instancia1.mochila, 878 as f64);
        assert_eq!(instancia1.items.len(), 20);
        assert_eq!(instancia1.items[0].beneficio, 46 as f64);
        assert_eq!(instancia1.items[19].peso, 92 as f64);

        assert_eq!(instancia2.mochila, 10000 as f64);
        assert_eq!(instancia2.items.len(), 23);
        assert_eq!(instancia2.items[0].beneficio, 980 as f64);
        assert_eq!(instancia2.items[22].peso, 983 as f64);

        assert_eq!(instancia3.mochila, 1008 as f64);
        assert_eq!(instancia3.items.len(), 200);
        assert_eq!(instancia3.items[0].beneficio, 416 as f64);
        assert_eq!(instancia3.items[199].peso, 343 as f64);

        assert_eq!(instancia4.mochila, 997 as f64);
        assert_eq!(instancia4.items.len(), 100);
        assert_eq!(instancia4.items[0].beneficio, 194 as f64);
        assert_eq!(instancia4.items[99].peso, 485 as f64);
    }
}