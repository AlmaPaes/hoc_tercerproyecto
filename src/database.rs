extern crate rusqlite;


use std::path::Path;
//use self::rusqlite::Connection;
use rusqlite::{Connection, NO_PARAMS};
use crate::instancia::*;
use std::process;


pub fn db_connection(num_instancia: i32) -> Connection {
    let instancia = format!("./instancias/instancia{}.db", num_instancia);
    let db_path = Path::new(&instancia);
    let connection = Connection::open(db_path).unwrap_or_else(|err| {
        eprintln!("Problema con la lectura de la base de datos: {:?}", err);
        process::exit(1);
    });
    connection
}

pub fn get_capacity(conn: &Connection) -> f64 {
    let mut stmt = conn.prepare("SELECT capacidad from knapsack")
                       .expect("Error al preparar conexión para obtener la capacidad de la mochila");

    let mut capacidad = stmt.query_map(NO_PARAMS, |row| { row.get(0)}).unwrap();
    capacidad.next().unwrap().unwrap()
}

pub fn get_items(conn: &Connection) -> Vec<Item> {
    let mut stmt = conn.prepare("SELECT beneficio,peso from item")
                       .expect("Error al preparar conexión para obtener los items.");
    
    let items_extract = stmt.query_map(NO_PARAMS, |row| {
        Ok(Item{
            beneficio: row.get(0)?,
            peso: row.get(1)?
        })
    }).unwrap();

    let mut items: Vec<Item> = Vec::new();
    for it in items_extract{
        let item = it.unwrap();
        items.push(item);
    }
    items
}