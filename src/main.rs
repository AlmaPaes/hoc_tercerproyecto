mod database;
mod instancia;
mod particula;
mod heuristica;

use std::env;
use std::process;
//use std::time::{Instant};
use std::sync::Arc;

use crossbeam::queue::ArrayQueue;

use crate::instancia::*;
use crate::heuristica::*;
use crate::particula::*;

fn get_argumentos(args: Vec<String>) -> Result<(Vec<u64>,i32), &'static str>{
    if args.len() < 2{
        return Err("Argumentos incorrectos: {semillas}");
    }

    let mut semillas: Vec<u64> = Vec::new();
    let mut instancia: i32 = 0;
    let mut iterador = args.split_first().unwrap().1.iter();
    let mut item = iterador.next();
    while item != None{
        let a = item.unwrap();
        if a.eq("-i"){
            match iterador.next().unwrap().parse::<i32>(){
                Ok(ins) => instancia = ins,
                Err(_) => {
                    println!("No contamos con esa instancia");
                    process::exit(1);
                } 
            }
        }
    
        else{
            match a.parse::<u64>(){
                Ok(seed) => semillas.push(seed),
                Err(_) =>{
                        println!("Semilla incorrecta. Sólo son aceptados números enteros");
                        process::exit(1);
                }
            }  
        }

        item = iterador.next();
    }
    
    Ok((semillas,instancia))
}


fn run_heuristica(semilla: u64, instancia: Arc<Instancia>) -> Particula {
    let num_particulas = 134;
    let constante1 = 2.0;
    let constante2 = 2.0;
    let velocidad_minima = 2.5;
    let velocidad_maxima = 3.7;
    let num_iteraciones = 5000;
    let inercia_inicial = 0.8;
    let inercia_final = 0.8;

    let mut optimizacion = Optimizacion::new(instancia.clone(),num_particulas,constante1,constante2,velocidad_minima,velocidad_maxima,
    inercia_inicial,inercia_final,num_iteraciones,semilla);

    let solucion = optimizacion.pso_heuristic();
    solucion
}

fn main() {
    //let now = Instant::now() ; 

    let args: Vec<String> = env::args().collect();
    let semillas: Vec<u64>; 
    let instancia: i32;
    match get_argumentos(args){
        Ok(datos) => {
            semillas = datos.0;
            instancia = datos.1;
        },
        Err(e) => {
            println!("{}",e);
            process::exit(1);
        }
    }

    let ejemplar = Arc::new(Instancia::new(instancia));
    let resultados = Arc::new(ArrayQueue::<(u64,Particula)>::new(semillas.len()));

    let handles: Vec<_> = semillas.into_iter().map(|seed| {
        let t_res = resultados.clone();
        let mi_ejemplar = ejemplar.clone();
        std::thread::spawn(move || {
            t_res.push((seed,run_heuristica(seed, mi_ejemplar)))
        })
    }).collect();

    for handle in handles {
        handle.join().unwrap().expect("Problemas con los hilos");
    }

    let final_results = Arc::try_unwrap(resultados).unwrap();
    while let Some(r) = final_results.pop() {
        println!("Mejor semilla: {:?}", r.0);
        println!("Mejor solucion: {:?}", r.1.mejor_posicion);
        println!("Costo: {}", r.1.mejor_valor);
    }


    //let new_now = Instant::now();
    //println!("{:?}", new_now.duration_since(now));
}
